function [x, y] = get_random_xy(n)
rng(1);
noise = randn(n, 1);
rng(2);
x = 10.*randn(n, 1);
rng(3);
a = 10.*rand(n, 1);
rng(4);
b = 10.*rand(n, 1);
y = max(a) + max(b)*x + noise;
end

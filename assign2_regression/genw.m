function w = genw(x, t, degree)
    bigx = genx(x, degree);
    bigy = geny(x, t, degree);
    w = bigx \ bigy; 
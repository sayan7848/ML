import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap


class Perceptron:
    '''Perceptron Classifier
    Parameters
    ----------
    n_iter : int
            number of epochs
    learn_rate : float
                learning rate
    thres : float
            threshold for bstep activation
    Attributes
    ----------
     w_  : 1d array
            list of weights
    errors : 1d array
            number of misclassification in each epoch
    '''
    def __init__(self, n_iter=10, learn_rate=0.01, thres=0.0):
        self.n_iter = n_iter
        self.learn_rate = learn_rate
        self.thres = thres

    def net_input(self, X):
        '''summation function
        Parameters
        ----------
        X : single row of inputs
        Returns
        -------
        dot protuct of weight and input
        '''
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def predict(self, X):
        '''prediction function
        Parameters
        ----------
        X : single row of input
        Returns
        -------
        class label (1 or -1)
        '''
        return np.where(self.net_input(X) >= self.thres, 1, -1)

    def fit(self, X, Y):
        '''train network
        Parameters
        ----------
        X : 2d input array
        Y : 1d output array
        Returns
        -------
        self
        '''
        self.w_ = np.zeros(X.shape[1] + 1)
        self.errors_ = []
        for _ in range(self.n_iter):
            errors = 0
            for xi, target in zip(X, Y):
                update = self.learn_rate * (target - self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0)
            self.errors_.append(errors)
        return self


def plot_decision_regions(X, Y, classifier, resolution=0.02, x_label='x_label', y_label='y_label'):
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'green', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(Y))])
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    # plot class samples
    for idx, cl in enumerate(np.unique(Y)):
        plt.scatter(x=X[Y == cl, 0], y=X[Y == cl, 1], alpha=0.8, c=cmap(idx), marker=markers[idx], label=cl)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='upper left')
    plt.show()


if __name__ == '__main__':
    df = pd.read_csv('iris.data', header=None)
    Y = df.iloc[0:100, 4].values
    Y = np.where(Y == 'Iris-setosa', -1, 1)
    X = df.iloc[0:100, [0, 2]].values
    '''plt.scatter(X[:50, 0], X[:50, 1], color='red', marker='o', label='setosa')
    plt.scatter(X[50:100, 0], X[50:100, 1], color='blue', marker='x', label='versicolor')
    plt.xlabel('pertal_length')
    plt.ylabel('sepal_length')
    plt.legend(loc='upper left')
    plt.show()'''
    network = Perceptron(n_iter=10, learn_rate=0.1, thres=0.0)
    network.fit(X, Y)
    '''plt.plot(range(1, len(network.errors_) + 1), network.errors_, marker='o', color='blue')
    plt.xlabel('epochs')
    plt.ylabel('misclassifications')
    plt.show()'''
    plot_decision_regions(X, Y, network, x_label='sepal length', y_label='petal length')

function y_line = myplot(w1, w2, x_line, plot_shape)
hold on;
y_line = w1 + w2 * x_line;
plot(x_line, y_line, plot_shape);
hold off;
end
%generate linearly spread values with random intercept and slope
[x, y] = get_random_xy(100);
%plot generated values
plot(x, y, 'g.');
%guess generated intercept and slope and print root mean square error
y_line1 = myplot(2, 4, x, 'y-');
disp(mean_square_error(y, y_line1));
y_line2 = myplot(2, 8, x, 'r-');
disp(mean_square_error(y, y_line2));
y_line3 = myplot(2, 10, x, 'b-');
disp(mean_square_error(y, y_line3));
%now work with dataset from uci machine learning library eg-iris dataset
iris = csvread('iris.csv', 0, 0, [0, 0, 1, end]);
disp(iris);
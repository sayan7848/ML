men = readtable('men.csv');
women = readtable('women.ods');
menx = men{1:27, 2};
meny = men{1:27, 3};
womenx = women{1:19, 2};
womeny = women{1:19, 3};

warning('off', 'all');

% %*******************linear**************
% [meny_predicted, rms_error] = get_predicted(menx, meny, 1);
% string = ['rms_error ' num2str(rms_error) ' for men for degree ' num2str(1)];
% disp(string);
% [womeny_predicted, rms_error] = get_predicted(womenx, womeny, 1);
% string = ['rms_error ' num2str(rms_error) ' for women for degree ' num2str(1)];
% disp(string);
% subplot(1, 3, 1);
% plot(menx, meny, '.b');
% title('Linear');
% hold on;
% plot(menx, meny_predicted, '-r');
% plot(womenx, womeny, '.g');
% plot(womenx, womeny_predicted, '-y');
% hold off;
% 
% %*******************quadratic**************
% [meny_predicted, rms_error] = get_predicted(menx, meny, 2);
% string = ['rms_error ' num2str(rms_error) ' for men for degree ' num2str(2)];
% disp(string);
% [womeny_predicted, rms_error] = get_predicted(womenx, womeny, 2);
% string = ['rms_error ' num2str(rms_error) ' for women for degree ' num2str(2)];
% disp(string);
% subplot(1, 3, 2);
% plot(menx, meny, '.b');
% title('Quadratic');
% hold on;
% plot(menx, meny_predicted, '-r');
% plot(womenx, womeny, '.g');
% plot(womenx, womeny_predicted, '-y');
% hold off;
% 
% %******************custom*******************
% [meny_predicted, rms_error] = get_predicted(menx, meny, 45);
% string = ['rms_error ' num2str(rms_error) ' for men for degree ' num2str(45)];
% disp(string);
% [womeny_predicted, rms_error] = get_predicted(womenx, womeny, 45);
% string = ['rms_error ' num2str(rms_error) ' for women for degree ' num2str(45)];
% disp(string);
% subplot(1, 3, 3);
% plot(menx, meny, '.b');
% title('degree 45');
% hold on;
% plot(menx, meny_predicted, '-r');
% plot(womenx, womeny, '.g');
% plot(womenx, womeny_predicted, '-y');
% hold off;

degree_list = [1 2 3 4 5 6 7 8];
%*****************kfold cross validation*************
[best_degree_men, ~] = crossvalidate(menx, meny, degree_list, 10, 'kfold', 1);
[best_degree_women, ~] = crossvalidate(womenx, womeny, degree_list, 10, 'kfold', 1);
[get_predicted_men, rmse_men] = get_predicted(menx, meny, best_degree_men);
[get_predicted_women, rmse_women] = get_predicted(womenx, womeny, best_degree_women);
subplot(1, 3, 1);
plot(menx, meny, '.r');
hold on;
plot(menx, get_predicted_men, '-b');
string = ['K-fold ' 'D_m ' num2str(best_degree_men) ' R_m ' num2str(rmse_men) ' D_w ' num2str(best_degree_women) ' R_w ' num2str(rmse_women)];
title(string);
plot(womenx, womeny, '.g');
plot(womenx, get_predicted_women, '-y');
hold off;
%*****************holdout****************************
[best_degree_men, ~] = crossvalidate(menx, meny, degree_list, 1, 'holdout', 50);
[best_degree_women, ~] = crossvalidate(womenx, womeny, degree_list, 1, 'holdout', 50);
[get_predicted_men, rmse_men] = get_predicted(menx, meny, best_degree_men);
[get_predicted_women, rmse_women] = get_predicted(womenx, womeny, best_degree_women);
subplot(1, 3, 2);
plot(menx, meny, '.r');
hold on;
plot(menx, get_predicted_men, '-b');
string = ['Holdout ' 'D_m ' num2str(best_degree_men) ' R_m ' num2str(rmse_men) ' D_w ' num2str(best_degree_women) ' R_w ' num2str(rmse_women)];
title(string);
plot(womenx, womeny, '.g');
plot(womenx, get_predicted_women, '-y');
hold off;
%***********leaveout*******************
[best_degree_men, ~] = crossvalidate(menx, meny, degree_list, 1, 'leaveout', 100);
[best_degree_women, ~] = crossvalidate(womenx, womeny, degree_list, 1, 'leaveout', 100);
[get_predicted_men, rmse_men] = get_predicted(menx, meny, best_degree_men);
[get_predicted_women, rmse_women] = get_predicted(womenx, womeny, best_degree_women);
subplot(1, 3, 3);
plot(menx, meny, '.r');
hold on;
plot(menx, get_predicted_men, '-b');
string = ['Leaveout ' 'D_m ' num2str(best_degree_men) ' R_m ' num2str(rmse_men) ' D_w ' num2str(best_degree_women) ' R_w ' num2str(rmse_women)];
title(string);
plot(womenx, womeny, '.g');
plot(womenx, get_predicted_women, '-y');
hold off;
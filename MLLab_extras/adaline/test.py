import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap


class AdalineGD:
    """Adaline Gradient Descent Classifier
    Parameters
    ----------
    n_iter : int
            number of epochs
    learn_rate : float
                learning rate
    thres : float
            threshold for bstep activation
    Attributes
    ----------
     w_  : 1d array
            list of weights
    errors : 1d array
            number of misclassification in each epoch
    """
    def __init__(self, n_iter=10, learn_rate=0.01, thres=0.0):
        self.n_iter = n_iter
        self.learn_rate = learn_rate
        self.thres = thres

    def net_input(self, X):
        """summation function
        Parameters
        ----------
        X : single row of inputs
        Returns
        -------
        dot protuct of weight and input
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def predict(self, X):
        """prediction function
        Parameters
        ----------
        X : single row of input
        Returns
        -------
        class label (1 or -1)
        """
        return np.where(self.net_input(X) >= self.thres, 1, -1)

    def fit(self, X, Y):
        """train network
        Parameters
        ----------
        X : 2d input array
        Y : 1d output array
        Returns
        -------
        self
        """
        self.w_ = np.zeros(X.shape[1] + 1)
        self.errors_ = []
        self.cost_ = []
        for _ in range(self.n_iter):
            output = self.net_input(X)
            errors = Y - output
            self.w_[1:] += self.learn_rate * X.T.dot(errors)
            self.w_[0] += self.learn_rate * errors.sum()
            cost = (errors**2).sum() / 2
            self.cost_.append(cost)
        return self


def plot_decision_regions(X, Y, classifier, resolution=0.02, x_label='x_label', y_label='y_label'):
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'green', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(Y))])
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    # plot class samples
    for idx, cl in enumerate(np.unique(Y)):
        plt.scatter(x=X[Y == cl, 0], y=X[Y == cl, 1], alpha=0.8, c=cmap(idx), marker=markers[idx], label=cl)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='upper left')
    plt.show()


if __name__ == '__main__':
    df = pd.read_csv('iris.data', header=None)
    Y = df.iloc[0:100, 4].values
    Y = np.where(Y == 'Iris-setosa', -1, 1)
    X = df.iloc[0:100, [0, 2]].values
    # standardization
    X_std = np.copy(X)
    X_std[:, 0] = (X_std[:, 0] - X_std[:, 0].mean()) / X_std[:, 0].std()
    X_std[:, 1] = (X_std[:, 1] - X_std[:, 1].mean()) / X_std[:, 1].std()
    # fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
    adal1 = AdalineGD(learn_rate=0.01).fit(X_std, Y)
    '''ax[0].plot(range(1, len(adal1.cost_) + 1), np.log10(adal1.cost_), marker='o')
    ax[0].set_xlabel('epochs')
    ax[0].set_ylabel('log(sum squared errors)')
    ax[0].set_title('learning rate = 0.01')
    adal2 = AdalineGD(learn_rate=0.0001).fit(X, Y)
    ax[1].plot(range(1, len(adal2.cost_) + 1), adal2.cost_, marker='o')
    ax[1].set_xlabel('epochs')
    ax[1].set_ylabel('sum squared errors')
    ax[1].set_title('learning rate = 0.0001')
    plt.show()'''
    plot_decision_regions(X_std, Y, classifier=adal1, x_label='sepal length', y_label='petal length')
    plt.plot(range(1, len(adal1.cost_) + 1), adal1.cost_, marker='o')
    plt.xlabel('epochs')
    plt.ylabel('sum squared errors')
    plt.show()

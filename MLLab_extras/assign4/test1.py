from math import *
from random import random


# initialize network
def initialize_network(num_input, num_hidden, num_output):
    network = []
    hidden_layer = [{"weights" : [random() for i in range(num_input + 1)]} for j in range(num_hidden)]
    output_layer = [{"weights" : [random() for i in range(num_hidden + 1)]} for j in range(num_output)]
    network.append(hidden_layer)
    network.append(output_layer)
    return network


# activation, bias is last one
def activate(weights, inputs):
    activation = weights[-1]
    for i in range(len(inputs) - 1):
        activation += weights[i] * inputs[i]
    return activation


# transfer the activation function, use sigmoid function
def transfer(activation):
    return 1.0 / (1.0 + exp(-activation))


# forward propagate input to network output
def forward_propagate(network, row):
    inputs = row
    for layer in network:
        new_inputs = []
        for neuron in layer:
            activation = activate(neuron['weights'], inputs)
            neuron['output'] = transfer(activation)
            new_inputs.append(neuron['output'])
        inputs = new_inputs
    return inputs


if __name__ == "__main__":
    network = initialize_network(2, 1, 2)
    for layers in network:
        print(layers)
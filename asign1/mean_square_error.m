function error = mean_square_error(Y, y)
error = 0;
for i=1:length(Y)
    elem1 = Y(i);
    elem2 = y(i);
    error = error + (elem1-elem2) * (elem1-elem2);
end
error = error / length(Y);
error = sqrt(error);
end

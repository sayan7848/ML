function percep(x,t,w)

    xmax = max(max(x));
    xmin = min(min(x));
    xmean = mean(mean(x));
    x(x>=xmean) = 1;
    x(x<xmean) = 0;
    t(t>=xmean) = 1;
    t(t<xmean) = 0;
    net = perceptron;
    net = train(net,x,t);
    view(net)
    y = net(w);
    y(y==1) = xmax;
    y(y==0) = xmin;
    disp(y);

end
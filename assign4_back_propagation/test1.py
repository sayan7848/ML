from random import random
from random import seed
from random import randrange
from math import exp
from csv import reader
import numpy as np
import matplotlib.pyplot as plt


# Load dataset from csv file
def load_csv(filename):
    try:
        dataset = []
        with open(filename, 'r') as file:
            csv_reader = reader(file)
            for row in csv_reader:
                if not row:
                    continue
                dataset.append(row)
        return dataset
    except IOError as err:
        print(str(err))


# Dataset read as string, need to convert it to float
def str_column_to_float(dataset, column):
    for row in dataset:
        row[column] = float(row[column].strip())


# Convert string column to integer, used for class columns
def str_column_to_int(dataset, column):
    class_values = [row[column] for row in dataset]
    unique = set(class_values)
    lookup = {}
    for i, value in enumerate(unique):
        lookup[value] = i
    for row in dataset:
        row[column] = lookup[row[column]]
    return lookup


# Min and max for columns in dataset
def dataset_minmax(dataset):
    min_val_col = np.min(dataset, axis=0)
    max_val_col = np.max(dataset, axis=0)
    return [min_val_col, max_val_col]


# Normalize dataset
def normalize_dataset(dataset, minmax):
    for row in dataset:
        for i in range(len(row) - 1):
            row[i] = (row[i] - minmax[0][i]) / (minmax[1][i] - minmax[0][i])


# Split dataset into folds
def cross_validation_split(dataset, n_folds):
    dataset_split = []
    dataset_copy = list(dataset)
    fold_size = int(len(dataset) / n_folds)
    for i in range(n_folds):
        fold = []
        while len(fold) < fold_size:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
        dataset_split.append(fold)
    return dataset_split


# Calculate accuracy of prediction
def accuracy_metric(actual, predicted):
    correct = 0
    for i in range(len(actual)):
        if actual[i] == predicted[i]:
            correct += 1
    return 100.0 - correct / float(len(actual)) * 100.0


# Evaluate algo
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
    folds = cross_validation_split(dataset, n_folds)
    scores = []
    for fold in folds:
        train_set = list(folds)
        train_set.remove(fold)
        train_set = sum(train_set, [])
        test_set = []
        for row in fold:
            row_copy = list(row)
            test_set.append(row_copy)
            row_copy[-1] = None
        predicted = algorithm(train_set, test_set, *args)
        actual = [row[-1] for row in fold]
        accuracy = accuracy_metric(actual, predicted)
        scores.append(accuracy)
    return scores


# Initialize network
def initialize_network(n_input, n_hidden, n_output):
    network = []
    hidden_layer = [{'weights': [random() for i in range(n_input + 1)]} for i in range(n_hidden)]
    output_layer = [{'weights': [random() for i in range(n_hidden + 1)]} for i in range(n_output)]
    network.append(hidden_layer)
    network.append(output_layer)
    return network


# Neuron activation for an input
def activate(weights, inputs):
    activation = weights[-1]  # bias
    # exclude bias from list of weights
    for i in range(len(weights) - 1):
        activation += weights[i] * inputs[i]
    return activation


# Transform to sigmoidal function
def transfer(activation):
    return 1.0 / (1.0 + exp(-activation))


# Forward propagate input to output
def forward_propagate(network, row):
    inputs = row
    for layer in network:
        new_inputs = []
        for neuron in layer:
            activation = activate(neuron['weights'], inputs)
            neuron['output'] = transfer(activation)
            new_inputs.append(neuron['output'])
        inputs = new_inputs
    return inputs


# Derivative for sigmoidal function
def transfer_derivative(output):
    return output * (1.0 - output)


# Back propagation step
def backward_propagate_error(network, expected):
    for i in reversed(range(len(network))):
        layer = network[i]
        errors = []
        if i != len(network) - 1:
            for j in range(len(layer)):
                error = 0.0
                for neuron in network[i + 1]:
                    error += (neuron['weights'][j] * neuron['delta'])
                errors.append(error)
        else:
            for j in range(len(layer)):
                neuron = layer[j]
                errors.append(expected[j] - neuron['output'])
        for j in range(len(layer)):
            neuron = layer[j]
            neuron['delta'] = errors[j] * transfer_derivative(neuron['output'])


# Update network weight with error
def update_weights(network, row, l_rate):
    for i in range(len(network)):
        # last value is class, so ignore it
        inputs = row[:-1]
        if i != 0:
            inputs = [neuron['output'] for neuron in network[i - 1]]
        for neuron in network[i]:
            for j in range(len(inputs)):
                neuron['weights'][j] += l_rate * neuron['delta'] * inputs[j]
            neuron['weights'][-1] += l_rate * neuron['delta']


# Train network
def train_network(network, train, l_rate, n_epoch, n_outputs, error_map=None):
    for epoch in range(n_epoch):
        sum_error = 0
        for row in train:
            outputs = forward_propagate(network, row)
            expected = [0 for i in range(n_outputs)]
            expected[row[-1]] = 1
            sum_error += sum([(expected[i] - outputs[i])**2 for i in range(len(expected))])
            backward_propagate_error(network, expected)
            update_weights(network, row, l_rate)
        if error_map is not None:
            if epoch not in error_map:
                error_map[epoch] = []
            error_map[epoch].append(sum_error)
        print('run=%d, error=%.3f' % (epoch, sum_error))


# Make a prediction with a network
def predict(network, row):
    outputs = forward_propagate(network, row)
    return outputs.index(max(outputs))


def back_propagation(train, test, l_rate, n_epoch, n_hidden, error_map=None):
    n_inputs = len(train[0]) - 1
    n_outputs = len(set([row[-1] for row in train]))
    network = initialize_network(n_inputs, n_hidden, n_outputs)
    train_network(network, train, l_rate, n_epoch, n_outputs, error_map)
    predictions = []
    for row in test:
        prediction = predict(network, row)
        predictions.append(prediction)
    return predictions


if __name__ == '__main__':
    seed(1)
    # load and normalize dataset
    filename = 'wine.csv'
    dataset = load_csv(filename)
    for i in range(len(dataset[0]) - 1):
        str_column_to_float(dataset, i)
    str_column_to_int(dataset, len(dataset[0]) - 1)
    minmax = dataset_minmax(dataset)
    normalize_dataset(dataset, minmax)
    # evaluate algorithm
    n_folds = 10
    l_rate = 0.5
    n_epoch = 500
    n_hidden_lst = [1, 2, 3, 4, 5]
    n_scores_lst = []
    error_map_list = []
    counter = 0
    for n_hidden in n_hidden_lst:
        error_map = {}
        scores = evaluate_algorithm(dataset, back_propagation, n_folds, l_rate, n_epoch, n_hidden, error_map)
        min_scores = sum(scores) / len(scores)
        n_scores_lst.append(min_scores)
        error_map_list.append(error_map)
    for error_map in error_map_list:
        x_list = []
        y_list = []
        for key, values in error_map.items():
            x_list.append(key)
            y_list.append(sum(values) / len(values))
        # Plot result
        plt.plot(x_list, y_list, label='num_hidden = ' + str(n_hidden_lst[counter]))
        counter += 1
    plt.ylabel('Error')
    plt.xlabel('Epoch')
    plt.title('Back Propagation Analysis')
    plt.legend()
    plt.show()

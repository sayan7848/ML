function Y = geny(x, t, degree)
    order = degree + 1;
    Y = zeros(order, 1);
    start = 0;
    for i = order:-1:1
        Y(i, 1) = sum((x.^start).*t);
        start = start + 1;
    end
end
function X = genx(x, degree)
    order = degree + 1;
    X = zeros(order, order);
    temp_mat = zeros(degree * 2);
    start = 0;
    for i = order:-1:1
       temp = start;
       for j = order:-1:1
           if temp_mat(temp + 1) == 0
                X(i, j) = sum(x.^temp);
                temp_mat(temp + 1) = X(i, j);
           else
                X(i, j) = temp_mat(temp + 1);
           end
           temp = temp + 1;
       end
       start = start + 1;
    end
end
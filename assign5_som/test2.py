from numpy import genfromtxt, linalg, zeros, apply_along_axis
from minisom import MiniSom
from pylab import plot, axis, show, pcolor, colorbar, bone


def do_som(iter=200, row=10, col=10):
    # read dataset
    data = genfromtxt('wine.csv', delimiter=',', usecols=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
    # normalise data
    data = apply_along_axis(lambda x: x / linalg.norm(x), 1, data)
    # initialization and training
    som = MiniSom(row, col, 13, sigma=1.0, learning_rate=0.5)
    som.random_weights_init(data)
    print("Training")
    som.train_random(data, iter)  # train with defualt 200 iterations
    print("ready")
    bone()
    pcolor(som.distance_map().T)  # distance map as background
    colorbar()
    # load labels
    target = genfromtxt('wine.csv', delimiter=',', usecols=(13), dtype=int)
    t = zeros(len(target), dtype=int)
    t[target == 1] = 0
    t[target == 2] = 1
    t[target == 3] = 2
    print(len(t))
    # different colors and markers for each label
    markers = ['o', 's', 'D']
    colors = ['r', 'g', 'b']
    for cnt, xx in enumerate(data):
        w = som.winner(xx)  # get the BMU
        # plot the BMU
        # print(cnt, t[cnt])
        plot(w[0] + .5, w[1] + .5, markers[t[cnt]], markerfacecolor='None', markeredgecolor=colors[t[cnt]], markersize=12, markeredgewidth=2)
    axis([0, som._weights.shape[0], 0, som._weights.shape[1]])
    show()  # show the figure


if __name__ == '__main__':
    do_som(10000, 15, 15)

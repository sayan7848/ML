function [ y, wb ] = linear_separibility( x1, x2, t )
%LINEAR_SEPARIBILITY Summary of this function goes here
%   Detailed explanation goes here
    mean1 = mean(x1);
    mean2 = mean(x2);
    mean3 = mean(t);
    x1(x1>mean1) = 1;
    x1(x1<=mean1) = 0;
    x2(x2>mean2) = 1;
    x2(x2<=mean2) = 0;
    t(t>mean3) = 1;
    t(t<=mean3) = 0;
    net = perceptron;
    x = [x1;x2];
    net1 = train(net, x, t);
    view(net1);
    y = net1([0, 1;0, 0]);
    wb = getwb(net1);
end


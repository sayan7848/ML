class HebianNN:
    def __init__(self, num_neurons=2, thres=2.0, bias=0.0, learn_rate=1.0, weights=None):
        self.num_neurons = num_neurons
        self.thres = thres
        self.bias = bias
        self.learn_rate = learn_rate
        if weights is None:
            self.weights = [0 for i in range(num_neurons)]
        else:
            self.weights = [weight for weight in weights]

    def train(self, lst):  # list of lists, last entry in each sublist is desired output
        for sub_lst in lst:
            sub_lst_len = len(sub_lst)
            for i in range(sub_lst_len - 1):
                self.weights[i] += self.learn_rate * sub_lst[i] * sub_lst[sub_lst_len - 1]
            self.bias += self.learn_rate * sub_lst[sub_lst_len - 1]

    def activate(self, value):
        if value >= self.thres:
            return 1
        return -1

    def test(self, inputs):
        sum = 0
        for i in range(len(inputs)):
            sum += inputs[i] * self.weights[i]
        return self.activate(sum + self.bias)


if __name__ == "__main__":
    hebian = HebianNN()
    hebian.train([[-1, -1, -1], [-1, 1, 1], [1, 1, 1]])
    print(hebian.test([1, -1]))

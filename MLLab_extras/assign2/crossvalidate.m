function [best_degree, least_rmse] = crossvalidate(x, t, degree, fold, method, repeat_times)
    num = numel(degree);
    least_rmse = flintmax;
    best_degree = degree(1);
    for i=1:num
        temp_least_rmse = flintmax;
        temp_best_degree = degree(i);
        for j=1:repeat_times
            size = numel(x);
            rmse = zeros(fold, 1);
            partition = cvpartition(size, method, fold);
            for k = 1:fold
                training = partition.training(k);
                [predicted, rmse(k)] = get_predicted(x(training, :), t(training, :), degree(i));
            end
            temp = mean(rmse);
            if temp < temp_least_rmse
                temp_least_rmse = temp;
                temp_best_degree = degree(i);
            end
        end
        if temp_least_rmse < least_rmse
            least_rmse = temp_least_rmse;
            best_degree = temp_best_degree;
        end
    end     
end
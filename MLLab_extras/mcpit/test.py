# x1    x2
# 0     0
# 0     1
# 1     0
# 1     1


class MCPit:
    def __init__(self, weights, bias, theta):
        self.weights = weights
        self.bias = bias
        self.theta = theta

    def threshold(self, input):
        if input >= self.theta:
            return 1
        return 0

    def fire(self, input_list):
        y = []
        for elem in input_list:
            sum = 0
            for i in range(len(self.weights)):
                sum += elem[i] * self.weights[i]
            y.append(self.threshold(sum + self.bias))
        return y


if __name__ == '__main__':
    input = [(0, 0), (0, 1), (1, 0), (1, 1)]
    output = [0, 1, 1, 1]  # or function
    weights = [1, 1]
    bias = 0.0
    theta = 0.5
    mcpit = MCPit(weights, bias, theta)
    output_predicted = mcpit.fire(input)
    for i in range(len(output)):
        print(output[i], output_predicted[i])

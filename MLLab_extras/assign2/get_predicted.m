function [t_predicted, rms_error] = get_predicted(x, t, degree)
    w = genw(x, t, degree);
    [row, column] = size(t);
    t_predicted = zeros(row,column);
    for i = 0:degree
        t_predicted = t_predicted + w(degree + 1 - i, 1) * x.^i;
    end
    rms_error = sqrt(mean((t_predicted - t).^2));
        